express = require('express')
cassandraDriver = require('cassandra-driver')
multer = require('multer')
upload = multer()
uuid = require('uuid/v4')
bodyParser = require('body-parser')
uuidValidate = require('uuid-validate')
sessionUtil = require('./session-util')
request = require('request')

app = express()
app.use(sessionUtil.sessionMiddleware())
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(logger);

uuidVersion = 4

cassandraMachine = "45.77.207.151"
cassandraLocalDataCenter = "datacenter1"
cassandraPort = 9042
cassandraKeySpace = "stackoverflow"

port = 8081


cassandraConnectionSettings = {
  contactPoints: [cassandraMachine+":"+cassandraPort],
  keyspace: cassandraKeySpace,
  localDataCenter: cassandraLocalDataCenter
}
cassClient = new cassandraDriver.Client(cassandraConnectionSettings)


validateSession = sessionUtil.validator.local()

function logger(req, res, next){
  console.log("Incoming Request")
  console.log(req.method + " "+req.url )
  next()
}

const typeMapping = {
  "jpeg" : "image",
  "jpg" : "image",
  "png" : "image",
  "gif" : "image",
  "tiff" : "image",
  "mpeg" : "video",
  "mp4" : "video"
}

app.get("/media/:id", function(req, res){
  var id = req.params["id"];
  if(!id){
    res.json({"status":"error", "error":"missing id"})
    return
  }


  var query = "SELECT ext, content FROM media WHERE id=?"
  cassClient.execute(query, [id], function(error, result){
    if(error || !result.rows[0]){
      res.status(400).json({"status":"error", "error":"not exist"})
    }
    else{
      var ext = result.rows[0]["ext"]
      res.setHeader("content-type", typeMapping[ext]+"/"+ext)

      res.end(result.rows[0]["content"])
    }
  })
})

app.post("/media/delete", function(req, res){
  var ids = []

  var reqIds = req.body["ids"];
  for( var i in reqIds ){
    if(uuidValidate(reqIds[i], uuidVersion))
      ids.push(reqIds[i])
  }
  if(ids.length<=0){
    res.json({"status":"OK"})
    return
  }

console.log(ids)
  var query = "DELETE FROM media WHERE id IN ?"
  cassClient.execute(query, [ids], function(error, result){
    if(error){
      res.status(400).json({"status":"error", "error":error})
    }
    else{
      res.json({"status":"OK"})
    }
  })
})

app.post("/media/user", function(req, res){
  var ids = []

  var reqIds = req.body["ids"];
  var username = req.body["username"]

  for( var i in reqIds ){
    if(uuidValidate(reqIds[i], uuidVersion))
      ids.push(reqIds[i])
  }
  if(ids.length<=0){
    res.json({"success": true})
    return
  }

  var updateQuery = "UPDATE media SET claimed=? WHERE id IN ?"
  cassClient.execute(updateQuery, [true, ids], function(error, result){
    res.json({"success":true})
    console.log(result)
  })
})

app.post("/media/claim", function(req, res){
  var ids = []

  var reqIds = req.body["ids"];
  var username = req.body["username"]
  for( var i in reqIds ){
    if(uuidValidate(reqIds[i], uuidVersion))
      ids.push(reqIds[i])
  }
  if(ids.length<=0){
    res.json({"success": true})
    return
  }

  var query = "SELECT username, claimed FROM media WHERE id IN ?"
  cassClient.execute(query, [ids], function(error, result){
    if(error){
      res.json({"status":"error", "error":error})
    }
    else{
      if(result.rows.length != ids.length){    // has some invalid ids
        res.json({"success": false, "error":"some ids were invalid"})
        return
      }
      var rows = result.rows;
      for(var i in rows){
        if(rows[i].claimed || rows[i].username != username){  // if any is used in other, or is not owned by user
          res.json({"success":false, "error":"cannot claim"})
          return
        }
      }

      var updateQuery = "UPDATE media SET claimed=? WHERE id IN ?"
      cassClient.execute(updateQuery, [true, ids], function(error, result){
        res.json({"success":true})
      })
    }
  })
})

app.post("/addmedia", validateSession, requireSession, upload.single("content"), async function(req, res){
  var filename = req.file.originalname;
  var content = req.file;
  if(!(filename && content)){
    //console.log(req.body)
    res.json({"status":"error", "error":"missing parameters"})
    return
  }

  var id = uuid()
  var ext = null
  var dotPosition = filename.lastIndexOf('.')
  if(dotPosition>0)
    ext = filename.substring(dotPosition+1)
  //var query = 'INSERT INTO media (id, username, claimed, ext, content) VALUES (?, ?, ?, ?, ?)'
  //var values = [id, req.username, false, ext, content.buffer]
  var query = 'INSERT INTO media (id, ext, content) VALUES (?, ?, ?)'
  var values = [id, ext, content.buffer]
//console.log(id)
//console.log(ext)
  await Promise.all([
    cassClient.execute(query, values),
    mediaLinkRequest(id, req.username)
  ])
  res.json({"status":"OK", "id": id})
/*
  cassClient.execute(query, values, function(error, result){
    if(error){
      res.status(400).json({"status":"error", "error":error})
    }
    else{
      res.json({"status":"OK", "id": id})
    }
  })
*/
})

function mediaLinkRequest(id, username){
  return new Promise(function(resolve){
    var headers = {}
    headers['Content-Type'] = 'application/json'
    let options = {
      body: JSON.stringify({ id: id, username: username }),
      method: 'POST',
      url: "http://149.28.226.23:80/link",
      headers: headers
    }
    request(options, function(error, cres, cbody){
      if(error)
        console.log(error)
      resolve()
    })
  })
}

function requireSession(req, res, next){
  req.username = sessionUtil.getSessionUsername(req)
  if(req.username==null){
    res.status(400).json({"status":"error", "error": "needs to be logged in. "})
  }
  else{
    next()
  }
}

app.post("/test", function(req, res){
	var query = "SELECT * FROM imgs"
	cassClient.execute(query, function(error, result){
		if(error)
      			res.json({"status":"error", "error":error})
		else
      			res.json({"status":"OK", "GOOD":result})
	})
})

app.listen(port, function(){
  console.log("Listening on port: "+port)
  console.log("Cassandra Settings:")
  console.log(cassandraConnectionSettings)
});
