proxy = require('./proxy.js')
session = require('express-session')
RedisStore = require('connect-redis')(session)
redisModule = require('redis')
require('dotenv').config()

redisOptions = {
  host: process.env.CACHE_HOST,
  port: process.env.CACHE_PORT
}
redisClient = redisModule.createClient(redisOptions)
redisClient.on('error', function (err) {
  console.log('////Redis failed to connect//// ' + err);
});
redisClient.on('connect', function (err) {
  console.log('Redis Connection Success');
});


var STATUS_OK = "OK"
var STATUS_ERROR = "error"

function validateSessionFar(req, res, next, sessionValidateEndpoint){
  proxy.forward(req, sessionValidateEndpoint, function(error, cres, cbody){
    if(error){
      next()
    }
    else{
      var jsonBody = JSON.parse(cbody);
      console.log("Auth status: "+jsonBody.status);

      if(jsonBody.status == STATUS_OK){
        req.headers["_session_username"] = jsonBody.session.username;
        next()
      }
      else{
        next()
      }
    }
  }, "GET")
}

function validateSessionLocal(req, res, next){
  if(req.session.user){
    req.headers["_session_username"] = req.session.user.username;
  }
  next();
}

validator = {
  local: function(){ return validateSessionLocal },
  distant: function(validationEndPoint) {
    return function(req, res, next){
      validateSessionFar(req, res, next, validationEndPoint)
    }
  }
}

function getSessionUsername(req){
  return req.headers["_session_username"]
}

function sessionMiddleware(){
  return session({
		secret: 'sessionsecret',
		name: 'session_id',
		cookie: { maxAge: 60*24*60*60*1000 },
		saveUninitialized: false,
		resave: false,
		store: new RedisStore( { client: redisClient } )
	})
}

function saveSessionUsername(req, username){
  req.session.user = { username: username }
}

function sessionAuthEndPointFunction(req, res){
	if(req.session.user){
		res.json({
			status: STATUS_OK,
			session: { username: req.session.user.username }
		})
	}
	else{
		res.json({
			status: STATUS_ERROR,
			error: "invalid session"
		})
	}
}

function getSessionModule(){
  return session
}

module.exports = {
  validator: validator,
  saveSessionUsername: saveSessionUsername,
  getSessionUsername: getSessionUsername,
  sessionMiddleware: function() { return sessionMiddleware() },
  sessionAuthEndPointFunction: sessionAuthEndPointFunction,
  getSessionModule: function() { return getSessionModule() }
}
