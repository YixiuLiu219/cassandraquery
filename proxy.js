setCookieParser = require('set-cookie-parser');
request = require('request');

const NAME_FIELD = "name";
const VALUE_FIELD = "value";

function transferResCookies(fromRes, toRes){
  var cookieMap = setCookieParser.parse(fromRes, {map:true});
  for(var key in cookieMap){
    var cookie = cookieMap[key];
    var options = {};
    for(var field in cookie){
      if(field != NAME_FIELD && field != VALUE_FIELD){
        options[field] = cookie[field];
      }
    }
    toRes.cookie(cookie[NAME_FIELD], cookie[VALUE_FIELD], options);
  }
}

function forward(req, url, callback, forcedMethod){

  var headers = {}
  headers['Content-Type'] = 'application/json'
  if(req.headers.cookie)
    headers['cookie'] = req.headers.cookie

  //for gateway pass, ---req.header("_session_username", getSessionUsername(req, fromStore=true))
  //                     proxy(req, "APIMachine")
  //                                         ---->API in another machine: username=getSessionUsername(req), if(!username)...
  //not used currently
  if(req.headers._session_username)
    headers['_session_username'] = req.headers._session_username

  var options = {
      body: JSON.stringify(req.body),
      method: forcedMethod? forcedMethod: req.method,
      headers: headers,
      url: url
  }

  request(options, callback);
}

function backward(fromRes, fromBody, toRes){
    transferResCookies(fromRes, toRes);
    toRes.setHeader('Content-Type', 'application/json');
    toRes.status(fromRes.statusCode)
    toRes.send(fromBody);
    toRes.end();
}

function proxy(req, res, url){
  forward(req, url, function(error, cres, cbody){
    if(error){
      console.log("Error happened");
      console.log(error);
    }
    backward(cres, cbody, res);
  })
}

module.exports = {
  transferResCookies: transferResCookies,
  forward: forward,
  backward: backward,
  proxy: proxy
}
